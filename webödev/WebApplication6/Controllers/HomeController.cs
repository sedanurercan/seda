﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace WebApplication6.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult ChangeCulture(String lang)
        {
            if (lang != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lang);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(lang);
            }
            HttpCookie cookie = new HttpCookie("Language");
            cookie.Value = lang;
            Response.Cookies.Add(cookie);
            return View("Index");
        }
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Farlar()
        {
            ViewBag.Message = "Far ";
            return View();
        }

        public ActionResult Rujlar()
        {
            ViewBag.Message = "Ruj ";
            return View();
        }
        public ActionResult Alliklar()
        {
            ViewBag.Message = "Allık ";
            return View();
        }
        public ActionResult Rimeller()
        {
            ViewBag.Message = "Rimel ";
            return View();
        }
        public ActionResult Fondoten()
        {
            ViewBag.Message = "Fondöten ve Kapatıcı ";
            return View();
        }
    }
}