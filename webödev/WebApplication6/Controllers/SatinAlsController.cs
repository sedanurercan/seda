﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Models;
using WebApplication6.Models.Detay;

namespace WebApplication6.Controllers
{
    [Authorize]
    public class SatinAlsController : Controller
    {
        private DetayContext db = new DetayContext();

        

        // GET: SatinAls/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SatinAl satinAl = db.SatinAls.Find(id);
            if (satinAl == null)
            {
                return HttpNotFound();
            }
            return View(satinAl);
        }
        [AllowAnonymous]
        // GET: SatinAls/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SatinAls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SatinAlID,AdSoyad,Email,EvAdresi,KrediKartiNo")] SatinAl satinAl)
        {
            if (ModelState.IsValid)
            {
                db.SatinAls.Add(satinAl);
                db.SaveChanges();
                return RedirectToAction("Details");
            }

            return View(satinAl);
        }
        [AllowAnonymous]

        // GET: SatinAls/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SatinAl satinAl = db.SatinAls.Find(id);
            if (satinAl == null)
            {
                return HttpNotFound();
            }
            return View(satinAl);
        }
        [AllowAnonymous]

        // POST: SatinAls/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SatinAlID,AdSoyad,Email,EvAdresi,KrediKartiNo")] SatinAl satinAl)
        {
            if (ModelState.IsValid)
            {
                db.Entry(satinAl).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(satinAl);
        }
        

        // GET: SatinAls/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SatinAl satinAl = db.SatinAls.Find(id);
            if (satinAl == null)
            {
                return HttpNotFound();
            }
            return View(satinAl);
        }

        // POST: SatinAls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SatinAl satinAl = db.SatinAls.Find(id);
            db.SatinAls.Remove(satinAl);
            db.SaveChanges();
            return RedirectToAction("Details");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
