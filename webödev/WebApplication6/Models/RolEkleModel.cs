﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class RolEkleModel
    {
        public string KullaniciAdi { get; set; }
        public string RolAdi { get; set; }
    }
}