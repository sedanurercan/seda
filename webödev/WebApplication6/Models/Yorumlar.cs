﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class Yorumlar
    {
        public int YorumlarID { get; set; }
        public string AdSoyad { get; set; }
        public string Baslik { get; set; }
        public string Icerik { get; set; }
    }
}