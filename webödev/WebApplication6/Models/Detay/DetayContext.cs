﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace WebApplication6.Models.Detay
{
    public class DetayContext:DbContext
    {
        public DetayContext()
            : base ("DetayContext")
        {

        }
        public DbSet<SatinAl> SatinAl { get; set; }
        public DbSet<Duyurular> Duyurular { get; set; }
        public DbSet<Yorumlar> Yorumlar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public System.Data.Entity.DbSet<WebApplication6.Models.SatinAl> SatinAls { get; set; }
    }
}