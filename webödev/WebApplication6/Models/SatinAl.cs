﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class SatinAl
    {
        public int SatinAlID { get; set; }
        public string AdSoyad { get; set; }
        public string Email { get; set; }
        public string EvAdresi { get; set; }
        public int KrediKartiNo { get; set; }
    }
}